Quellen der verwendeten Icons: 

arrow-down.png:
https://materialdesignicons.com/icon/arrow-down
Lizenz: SIL Open Font License 1.1
Nähere Informationen unter: https://github.com/Templarian/MaterialDesign/blob/master/license.txt

arrow-up.png:
https://materialdesignicons.com/icon/arrow-up
Lizenz: SIL Open Font License 1.1
Nähere Informationen unter: https://github.com/Templarian/MaterialDesign/blob/master/license.txt