/**
* @module vigenereModule
* @memberof App
* @returns {function} start Startet das Modul fuer das Vigenére-Verfahren
*/
var vigenereModule = (function(){
/** Erzeugt das Layout und die Funktionalitäten für das Vigenére-Verfahren 
 * @function start 
 */
 function start(){
    
    var vigTitel = new createjs.Text("Vigenére-Verfahren", largeFont, blue);
    var vigBound = vigTitel.getBounds();
    vigTitel.x = (canvas.width/2 - vigBound.width/2) / scale;
    vigTitel.y = (vigBound.height/4) / scale;
    stage.addChild(vigTitel);
    
     /** Vigenére-Quadrat erzeugen */
    var vQuadrat = new VigenereShape();
    stage.addChild(vQuadrat.vigenereContainer);
     
     /** Input Felder mit Überschrift */
    var klartxt = new createjs.Text("Klartext:", mediumFont, tuerkis);
    var kBound = klartxt.getBounds();
    klartxt.text.textAlign = "center";
    klartxt.textBaseline = "middle";
    klartxt.x = (canvas.width/4 - kBound.width/2 - offset) / scale;
    klartxt.y = (canvas.height/(1.4)) / scale;
    stage.addChild(klartxt);
    
    var inputField = document.createElement("input");
    inputField.style.position = "absolute";
    inputField.style.left = 0;
    inputField.style.top = 0;
    inputField.setAttribute("z-index", "1");
    inputField.setAttribute("type","text");
    inputField.size = 120;
    inputField.style.width = "50%";
    inputField.id = "inputField";
    inputField.value = ""; 
    
    canvas.parentNode.appendChild(inputField);
    var inputFeld = new createjs.DOMElement(inputField);
    inputFeld.x = (canvas.width/4 + 3*offset) / scale; 
    inputFeld.y = (canvas.height/(1.4) - offset/2) / scale;;
    stage.addChild(inputFeld);
   
    var schluessel = new createjs.Text("Schlüssel:", mediumFont, black);
    sBound = schluessel.getBounds();
    schluessel.text.textAlign = "center";
    schluessel.textBaseline = "middle";
    schluessel.x = (canvas.width/4 - sBound.width/2 - offset) / scale;
    schluessel.y = (canvas.height/(1.2))/scale;
    stage.addChild(schluessel);
    
    var wert = document.createElement("input");
    wert.style.position = "absolute";
    wert.style.left = 0;
    wert.style.top = 0;
    wert.setAttribute("z-index", "1");
    wert.setAttribute("type","text");
    wert.size = 10;
    wert.style.width = "50%";
    wert.id = "key";
    wert.value = "";
    
    canvas.parentNode.appendChild(wert);
    var wertFeld = new createjs.DOMElement(wert);
    wertFeld.x = (canvas.width/4 + 3*offset) / scale; 
    wertFeld.y = (canvas.height/(1.2) - offset/2) / scale;
    stage.addChild(wertFeld);
    
    var verschluesselt = new createjs.Text("Geheimtext:", mediumFont, red);
    var veBound = verschluesselt.getBounds();
    verschluesselt.text.textAlign = "center";
    verschluesselt.textBaseline = "middle";
    verschluesselt.x = (canvas.width/4 - veBound.width/2 - offset) / scale;
    verschluesselt.y = (canvas.height/(1.3)) / scale;
    stage.addChild(verschluesselt);
    
    var output = document.createElement("input");
    output.style.position = "absolute";
    output.style.left = 0;
    output.style.top = 0;
    output.setAttribute("z-index", "1");
    output.setAttribute("type","text");
    output.size = 120;
    output.style.width = "50%";
    output.id = "output";
    output.disabled = true;
    output.value = ""; 
    
    canvas.parentNode.appendChild(output);
    var outputFeld = new createjs.DOMElement(output);
    outputFeld.x = (canvas.width/4 + 3*offset) / scale; 
    outputFeld.y = (canvas.height/(1.3) - offset/2) / scale;
    stage.addChild(outputFeld);
    
    /** Tausch-Button zum tauschen von Klar- und Geheimtext */
    var tauschBtn = new SwitchButton();
    stage.addChild(tauschBtn.switchContainer);
    
    /** Verschlüsseln- und Entschlüsseln-Button */
    var buttonEncryp = new EncrypBtn();
    stage.addChild(buttonEncryp.encrypBtnContainer);
    
    var buttonDecryp = new DecrypBtn();
    stage.addChild(buttonDecryp.decrypBtnContainer);
    
    
    /** Tauscht Klar- und Geheimtext bei drücken des Tausch-Buttons 
    * @param {event} evt Click-Event auf den Tauschen-Button
    */
    tauschBtn.switchContainer.on("click", function(evt){
        clickCounter++;
        
        var klarText = document.getElementById("inputField").value;
        var geheimText = document.getElementById("output").value;
        document.getElementById("inputField").value = geheimText;
        document.getElementById("output").value = klarText;
        
        if(clickCounter % 2 === 1){
            klartxt.text = "Geheimtext:";
            klartxt.color = red;
            klartxt.x = verschluesselt.x;
        
            verschluesselt.text = "Klartext:";
            verschluesselt.color = tuerkis;
            verschluesselt.x = klartxt.x;
        } else {
            klartxt.text = "Klartext:";
            klartxt.color = tuerkis;
            klartxt.x = klartxt.x;
        
            verschluesselt.text = "Geheimtext:";
            verschluesselt.color = red;
            verschluesselt.x = verschluesselt.x;
        }
    });
   
     /**Prueft ob Weiter-Button noch vorhanden ist oder noch eine Animation läuft,
    wenn Verschlüsseln-Button erneut gedrückt wird. Falls ja wird der Weiter-Button
    entfernt und die Animation beendet
    * @param {event} evt Click-Event auf den Verschlüsseln-Button
    */
    buttonEncryp.encrypBtnContainer.on("click", function(evt){
    
        if(animateContainer){
           animateContainer.animationContainer.removeAllChildren();
           stage.removeChild(animateContainer.animationContainer);
        }
        if(animateContainer2){
            animateContainer2.animationContainer.removeAllChildren();
            stage.removeChild(animateContainer2.animationContainer);
        }  
        if(weiterBtn){
            stage.removeChild(weiterBtn.weiterContainer);
        }
        stage.update();
    });
    
     /** Event-Handler für clicken des Verschlüsseln-Buttons. 
        Ruft die Funktion für die Überprüfung des gedrückten Buttons auf.
    */
    buttonEncryp.encrypBtnContainer.addEventListener("click", handleButton);
    
     /**Prüft ob Weiter-Button noch vorhanden ist oder noch eine Animation läuft,
    wenn Entschlüsseln-Button erneut gedrückt wird. Falls ja wird der Weiter-Button
    entfernt und die Animation beendet
    * @param {event} evt Click-Event auf den Entschlüsseln-Button
    */
    buttonDecryp.decrypBtnContainer.on("click", function(evt){
   
         if(animateContainer){
            animateContainer.animationContainer.removeAllChildren();
            stage.removeChild(animateContainer.animationContainer);
          }
          if(animateContainer2){
            animateContainer2.animationContainer.removeAllChildren();
            stage.removeChild(animateContainer2.animationContainer);
         }
          if(weiterBtn){
            stage.removeChild(weiterBtn.weiterContainer);
         }
        stage.update();
    });
    
     /** Event-Handler für clicken des Entschlüsseln-Buttons. 
        Ruft die Funktion für die Überprüfung des gedrückten Buttons auf.
    */
    buttonDecryp.decrypBtnContainer.addEventListener("click", handleButton);
    
    stage.update();
 }
     /** Gibt die Startfunktion beim Vigenére-Modul zurück
    * @returns {function} start Funktion zum starten des Vigenére-Moduls
    */
    return{
        start:start
    };
})();

/**
*Prüft ob Ver- oder Entschlüsseln-Button gedrückt wurde und 
ruft Funktion für das Vorbereiten der Eingabe auf.
* @function handleButton
* @param {event} evt Click-Event auf den Ver- oder Entschlüsseln-Button
*/
function handleButton(evt){
    var toDescrypt = false;
    if(evt.currentTarget.name == "verBtn"){
       chars = [];
       toDescrypt = false; 
       document.getElementById("output").value = "";
       checkVerschluesseln(toDescrypt);
    }
    else if(evt.currentTarget.name == "entBtn"){
        chars = [];
        toDescrypt = true;
        document.getElementById("output").value = "";
        checkVerschluesseln(toDescrypt);
    }
}
