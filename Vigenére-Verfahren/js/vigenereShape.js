/** Erzeugt die Grafik für das Vigenére-Quadrat
* @class
*/
function VigenereShape(){
    chars = [];
    for(i = 0; i < labelCaesar.length; i++){
        chars.push(labelCaesar[i]);
    }
    this.vigenereContainer = new createjs.Container();
    
    this.keyShape = new createjs.Shape();
    this.keyShape.graphics.beginStroke(black).beginFill(yellow).drawRect(0, 0, textHeight, textWidth);
    this.keyShape.y += offset;
    
    this.textShape = new createjs.Shape();
    this.textShape.graphics.beginStroke(black).beginFill(tuerkis).drawRect(0, 0, textWidth, textHeight);
    this.textShape.x += offset; 
    
    var keyLabel = new createjs.Text("Schlüssel", mediumFont, black);
    var kb = keyLabel.getBounds();
    keyLabel.x = (-2) * textHeight;
    keyLabel.y = (textWidth + kb.width) / 2;
    keyLabel.rotation = 270;
    this.vigenereContainer.addChild(keyLabel);
    
    var plainLabel = new createjs.Text("Klartext", mediumFont, black);
    var pb = plainLabel.getBounds();
    plainLabel.x = (textWidth - pb.width) / 2;
    plainLabel.y = - (2) * textHeight;
    this.vigenereContainer.addChild(plainLabel);
    
    this.vigSquare = new createjs.Shape();
    this.vigSquare.graphics.beginStroke(black).beginFill(transparentRed).drawRect(0, 0, squareWidth, squareHeight);
    this.vigSquare.x = textHeight;
    this.vigSquare.y = textHeight;
    
    this.vigenereContainer.addChild(this.vigSquare);
    
    var spacing = 14.25 * scale;
    
    this.vigenereContainer.addChild(this.keyShape, this.textShape);
    
    /** Setzt Text auf das Vigenére-Quadrat */
    for(var index = 0; index < labelCaesar.length; index++){
        this.keyChar = new createjs.Text();
        this.keyChar.text = labelCaesar[index];
        this.keyChar.font = smallFont;
        var kBound = this.keyChar.getBounds();
        this.keyChar.textAlign = "center";
        this.keyChar.textBaseline = "middle";
        this.keyChar.x = textHeight/2;
        this.keyChar.y += this.keyShape.y + (index * spacing + kBound.height/2 + offset/4);
        if(keyX.length < labelCaesar.length && keyY.length < labelCaesar.length){
            keyX.push(this.keyChar.x);
            keyY.push(this.keyChar.y);
        }
        this.vigenereContainer.addChild(this.keyChar);
    }
    
    for(var index = 0; index < labelCaesar.length; index++){
        this.plainChar = new createjs.Text();
        this.plainChar.text = labelCaesar[index];
        this.plainChar.font = smallFont;
        var pBound = this.plainChar.getBounds();
        this.plainChar.textAlign = "center";
        this.plainChar.textBaseline = "middle";
        this.plainChar.x += textHeight + (index * spacing + pBound.width/2 + offset/4);
        this.plainChar.y = textHeight/2;
         if(textX.length < labelCaesar.length && textY.length < labelCaesar.length){
           textX.push(this.plainChar.x);
           textY.push(this.plainChar.y);
        }
        this.vigenereContainer.addChild(this.plainChar);
    }
    
    for(var i = 0; i < labelCaesar.length; i++){ 
        squareChars[i] = new Array(25);
        /** Verschiebe Alphabet */
        if(i >= 1) {
               var shifted = chars.shift();
               chars.push(shifted);
            }
        for(var j = 0; j < labelCaesar.length; j++){
            this.squareChar = new createjs.Text();
            this.squareChar.text = chars[j];
            this.squareChar.font = smallFont;
            var sBound = this.plainChar.getBounds();
            this.squareChar.textAlign = "center";
            this.squareChar.textBaseline = "middle";
            this.squareChar.x += textHeight + (j * spacing + pBound.width/2 + offset/4);
            squareChars[i][j] = this.squareChar.x;
            this.squareChar.y = (1.5)*textHeight + (i * spacing);
            this.vigenereContainer.addChild(this.squareChar);
    }
  }
    
    this.vigenereContainer.x = (canvas.width/2 - squareWidth/3 - textHeight/3) / scale;
    this.vigenereContainer.y = (canvas.height/8) / scale;
    this.vigenereContainer.scaleX = 0.8;
    this.vigenereContainer.scaleY = 0.8;
    
    this.animationContainer = new createjs.Container();
    this.animationContainer.x = this.vigenereContainer.x; 
    this.animationContainer.y = this.vigenereContainer.y;
    this.animationContainer.scaleX = 0.8;
    this.animationContainer.scaleY = 0.8;  
}

