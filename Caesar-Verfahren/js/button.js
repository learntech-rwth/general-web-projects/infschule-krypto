/**
* Erzeugt einen neuen Zurück-Button.
* Wird benötigt, falls später noch weitere Module und ein Hauptmenue dazukommen
* @class
*/
function ZurueckBtn(){
    
    this.zButton = new createjs.Container();
    
    this.zurueckBtn = new createjs.Shape();
    this.zurueckBtn.graphics.beginStroke(black).beginFill(darkgrey).drawRoundRect(0, 0, zurueckBtnWidth, zurueckBtnHeight, 4);
    this.zButton.addChild(this.zurueckBtn);
    
    this.label = new createjs.Text("zurück", smallFont, black);
    this.label.name = "label";
    this.label.textAlign = "center";
    this.label.textBaseline = "middle";
    this.label.x = zurueckBtnWidth/2;
    this.label.y = zurueckBtnHeight/2;
    this.zButton.addChild(this.label)
    
    this.zButton.name = "zurueckBtn";
    this.zButton.x = 1 / scale; 
    this.zButton.y = 1 / scale; 
}


/**
* Erzeugt einen neuen Verschlüsseln-Button
* @class
*/
function EncrypBtn(){
    
    this.encrypBtnContainer = new createjs.Container();
    
    this.encryBtn = new createjs.Shape();
    this.encryBtn.graphics.beginStroke(black).beginFill(darkgrey).drawRoundRect(0, 0, crypBtnWidth, crypBtnHeight, 4);
    this.encrypBtnContainer.addChild(this.encryBtn);
    
    this.labelVer = new createjs.Text("Verschlüsseln", smallFont, black);
    this.labelVer.textAlign = "center";
    this.labelVer.textBaseline = "middle";
    this.labelVer.x = crypBtnWidth/2;
    this.labelVer.y = crypBtnHeight/2;
    this.encrypBtnContainer.addChild(this.labelVer);
    
    
    this.encrypBtnContainer.name = "verBtn";
    this.encrypBtnContainer.x = (canvas.width/4 - crypBtnWidth/2) / scale;;
    this.encrypBtnContainer.y = (canvas.height/(1.1)) / scale;
}

/**
* Erzeugt einen neuen Entschlüsseln-Button
* @class
*/
function DecrypBtn(){
    
    this.decrypBtnContainer = new createjs.Container();
    
    this.decrypBtn = new createjs.Shape();
    this.decrypBtn.graphics.beginStroke(black).beginFill(darkgrey).drawRoundRect(0, 0, crypBtnWidth, crypBtnHeight, 4);
    this.decrypBtnContainer.addChild(this.decrypBtn);
    
    this.labelEnt = new createjs.Text("Entschlüsseln", smallFont, black);
    this.labelEnt.textAlign = "center";
    this.labelEnt.textBaseline = "middle";
    this.labelEnt.x = crypBtnWidth/2;
    this.labelEnt.y = crypBtnHeight/2;
    this.decrypBtnContainer.addChild(this.labelEnt);
    
   
    this.decrypBtnContainer.name = "entBtn"
    this.decrypBtnContainer.x = (canvas.width/4 + crypBtnWidth/2 + offset) / scale;
    this.decrypBtnContainer.y = (canvas.height/(1.1)) / scale;
}

/**
* Erzeugt einen neuen Weiter-Button
* @class
*/
function WeiterBtn(){
    
    this.weiterContainer = new createjs.Container();
    
    this.weiterBtn = new createjs.Shape();
    this.weiterBtn.graphics.beginStroke(black).beginFill(darkgrey).drawRoundRect(0, 0, zurueckBtnWidth, zurueckBtnHeight, 4);
    this.weiterContainer.addChild(this.weiterBtn);
    
    this.weiterLabel = new createjs.Text("weiter", smallFont, black);
    this.weiterLabel.textAlign = "center";
    this.weiterLabel.textBaseline = "middle";
    this.weiterLabel.x = zurueckBtnWidth/2;
    this.weiterLabel.y = zurueckBtnHeight/2;
    this.weiterContainer.addChild(this.weiterLabel);
    
    
    this.weiterContainer.name = "weiterBtn";
    this.weiterContainer.x = 0;
    this.weiterContainer.y = 0;
}

/**
* Erzeugt einen neuen Button zum tauschen von Klar- und Geheimtext
* @class
*/
function SwitchButton(){
    this.switchBtn = new createjs.Shape();
    this.switchBtn.graphics.beginStroke(black).beginFill(darkgrey).drawRoundRect(0, 0, switchBtnWidth, switchBtnHeight, 4);
    this.switchBtn.x = (canvas.width/(1.3) + switchBtnWidth) / scale;
    this.switchBtn.y = (canvas.height/(1.3) - switchBtnHeight/2) / scale;
    
    this.arrowUp = new createjs.Bitmap("images/arrow-up.png");
    this.arrowUp.x = this.switchBtn.x + offset/8;
    this.arrowUp.y = this.switchBtn.y + offset/8;
    this.arrowUp.scaleX = scale;
    this.arrowUp.scaleY = scale;
    
    this.arrowDown = new createjs.Bitmap("images/arrow-down.png");
    this.arrowDown.x = this.switchBtn.x + offset * (1.2);
    this.arrowDown.y = this.switchBtn.y + offset/4;
    this.arrowDown.scaleX = scale;
    this.arrowDown.scaleY = scale;
    
    this.switchContainer = new createjs.Container();
    this.switchContainer.name = "tauschBtn";
    this.switchContainer.x = 0;
    this.switchContainer.y = 0;
    this.switchContainer.addChild(this.switchBtn, this.arrowUp, this.arrowDown);
}

/**
* Erzeugt einen neuen Button zum prüfen des Schlüssels beim One-Time-Pad-Verfahren
* @class
*/
function PruefButton(){
    
    this.pruefButton = new createjs.Container();
    
    this.pruefShape = new createjs.Shape();
    this.pruefShape.graphics.beginStroke(black).beginFill(darkgrey).drawRoundRect(0, 0, switchBtnWidth + offset/2, zurueckBtnHeight, 4);
    this.pruefButton.addChild(this.pruefShape);
    
    this.pruefLabel = new createjs.Text("Schlüssel \n \n prüfen", tinyFont, black);
    var bound = this.pruefLabel.getBounds();
    this.pruefLabel.textAlign = "center";
    this.pruefLabel.textBaseline = "middle";
    this.pruefLabel.x = switchBtnWidth/2 + bound.width/8;
    this.pruefLabel.y = zurueckBtnHeight/2 - bound.height/3;
    this.pruefButton.addChild(this.pruefLabel);
    
    this.pruefButton.name = "prüfen";
    this.pruefButton.x = (canvas.width/(1.3) + switchBtnWidth) / scale;
    this.pruefButton.y = (canvas.height/(1.2) - zurueckBtnHeight/2) / scale;
}