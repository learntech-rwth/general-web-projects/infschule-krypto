
/**
* @module caesarModule
* @memberof App
* @returns {function} start Startet das Modul fuer das Caesar-Verfahren
*/
var caesarModule = (function(){
/**
* Erzeugt das Layout für das Caesar-Verfahren und stellt verschiedene Funktionalitäten bereit
* @function start
*/
 function start(){
    for(i = 0; i < labelCaesar.length; i++){
        ringChars.push(labelCaesar[i]);
    }
     
    var caesarTitel = new createjs.Text("Caesar-Verfahren", largeFont, blue);
    var titelBound = caesarTitel.getBounds();
    caesarTitel.x = (canvas.width/2 - titelBound.width/2) / scale;
    caesarTitel.y = (titelBound.height/4) / scale;
    stage.addChild(caesarTitel);
    
    var plainTextRing = new CaesarShape();
    stage.addChild(plainTextRing.outerTextContainer);
    
    var crypRing = new CaesarShape();
    stage.addChild(crypRing.innerTextContainer);
    
    var empty = new CaesarShape();
    stage.addChild(empty.container);
    
    /** Behandelt Rotation der Caesar-Scheibe bei drücken der linken Maustaste
    * Anmerkung: Die Umsetzung ist dem mtlg-datba Projekt des Softwareprojekt Praktikums, 
       E-Learnig in der Schule, entnommen an dem ich im Wintersemester 2016/2017 teilgenommen habe 
       @param {event} evt Click-Event, wird übergeben wenn der linke Mauszeiger gehalten wird
    */
    crypRing.innerTextContainer.on("mousedown", function (evt) {
        var ct = evt.currentTarget;
        var offset = rotationAngle(ct.x, ct.y, evt.stageX, evt.stageY);
        
        ct.rotationOffset = (offset - ct.rotation);
    });
    
     /** Behandelt Rotation der Caesar-Scheibe bei touch auf dem inneren Ring der Caesar-Scheibe
    * Anmerkung: Die Umsetzung ist dem mtlg-datba Projekt des Softwareprojekt Praktikums, 
       E-Learnig in der Schule, entnommen an dem ich im Wintersemester 2016/2017 teilgenommen habe 
       @param {event} evt Touch-Event, wird bei Touch auf dem inneren Ring der Caesar-Scheibe übergeben
    */
	crypRing.innerTextContainer.on("pressmove", function(evt){
        labelRotation = [];
        var ct = evt.currentTarget;
        var offset = rotationAngle(ct.x, ct.y, evt.stageX, evt.stageY);
        
        ct.rotation = (offset - ct.rotationOffset);
        
        for(i = 0; i < initialRotation.length; i++){
            labelRotation.push(initialRotation[i]);
        }
        
        stage.update();
    });
    
    /** Berechnet Abstand zum nächsten Buchstabenfeld, Wert für die Verschiebung 
     des Alphabets und ermöglicht einrasten der Scheibe.
    * @param {event} evt Loslassen-Event beim inneren Ring der Caesar-Scheibe
    */
    crypRing.innerTextContainer.on("pressup", function(evt){
        var ct = evt.currentTarget;
        var spacing = (360 / 26);
        var modRes = ct.rotation % spacing;
        if(modRes < (spacing * 0.5)){
            ct.rotation = ct.rotation - modRes;
        }
        else if(modRes > (spacing * 0.5)){
            ct.rotation = ct.rotation + (spacing - modRes);
        }
        
        for(i = 0; i < initialRotation.length; i++){
            labelRotation[i] += ct.rotation % 360;
        }
        
        // Berechne Verschiebung fuer das Caesar-Verfahren
        var verschiebung = labelRotation[0] / spacing;
        if(verschiebung > 0){
            caesarVerschiebung = Math.round(verschiebung % 26);
            document.getElementById("verschiebung").value = caesarVerschiebung;
        }
        else if(verschiebung < 0){
            caesarVerschiebung = Math.round((26 + verschiebung) % 26);
            document.getElementById("verschiebung").value = caesarVerschiebung;
        } else {
            caesarVerschiebung = 0;
            document.getElementById("verschiebung").value = caesarVerschiebung;
        }
    });

    
    
    /**Input Felder mit Überschrift */
    var klartxt = new createjs.Text("Klartext:", mediumFont, tuerkis);
    var kBound = klartxt.getBounds();
    klartxt.text.textAlign = "center";
    klartxt.textBaseline = "middle";
    klartxt.x = (canvas.width/4 - kBound.width/2 - offset) / scale;
    klartxt.y = (canvas.height/(1.4)) / scale;
    stage.addChild(klartxt);
    
    var inputField = document.createElement("input");
    inputField.style.position = "absolute";
    inputField.style.left = 0;
    inputField.style.top = 0;
    inputField.setAttribute("z-index", "1");
    inputField.setAttribute("type","text");
    inputField.size = 120;
    inputField.style.width = "50%";
    inputField.id = "inputField";
    inputField.value = ""; 
    
    canvas.parentNode.appendChild(inputField);
    var inputFeld = new createjs.DOMElement(inputField);
    inputFeld.x = (canvas.width/4 + 3*offset) / scale; 
    inputFeld.y = (canvas.height/(1.4) - offset/2) / scale;
    stage.addChild(inputFeld);
   
    var verschiebung = new createjs.Text("Verschiebung:", mediumFont, black);
    var veb = verschiebung.getBounds();
    verschiebung.text.textAlign = "center";
    verschiebung.textBaseline = "middle";
    verschiebung.x = (canvas.width/4 - veb.width/2 - offset) / scale;
    verschiebung.y = (canvas.height/(1.2)) / scale;
    stage.addChild(verschiebung);
    
    var wert = document.createElement("input");
    wert.style.position = "absolute";
    wert.style.left = 0;
    wert.style.top = 0;
    wert.setAttribute("z-index", "1");
    wert.setAttribute("type","text");
    wert.size = 10;
    wert.id = "verschiebung";
    wert.disabled = true;
    wert.value = "0"; 
    
    canvas.parentNode.appendChild(wert);
    var wertFeld = new createjs.DOMElement(wert);
    wertFeld.x = (canvas.width/4 + 3*offset) / scale;
    wertFeld.y = (canvas.height/(1.2) - offset/2) / scale;
    stage.addChild(wertFeld);
    
    var verschluesselt = new createjs.Text("Geheimtext:", mediumFont, red);
    vb = verschluesselt.getBounds();
    verschluesselt.text.textAlign = "center";
    verschluesselt.textBaseline = "middle";
    verschluesselt.x = (canvas.width/4 - vb.width/2 - offset) / scale;
    verschluesselt.y = (canvas.height/(1.3))/scale;
    stage.addChild(verschluesselt);
    
    var output = document.createElement("input");
    output.style.position = "absolute";
    output.style.left = 0;
    output.style.top = 0;
    output.setAttribute("z-index", "1");
    output.setAttribute("type","text");
    output.size = 120;
    output.style.width = "50%";
    output.id = "output";
    output.disabled = true;
    output.value = ""; 
    
    canvas.parentNode.appendChild(output);
    var outputFeld = new createjs.DOMElement(output);
    outputFeld.x = (canvas.width/4 + 3*offset) / scale; 
    outputFeld.y = (canvas.height/(1.3) - offset/2) / scale;
    stage.addChild(outputFeld);
    
    /** Tausch-Button */
    var tauschBtn = new SwitchButton();
    stage.addChild(tauschBtn.switchContainer);
    
    /** Verschlüsseln- und Entschlüsseln-Button */
    var buttonEncryp = new EncrypBtn();
    stage.addChild(buttonEncryp.encrypBtnContainer);
    
    var buttonDecryp = new DecrypBtn();
    stage.addChild(buttonDecryp.decrypBtnContainer);
    
    /** Tauscht Klar- und Geheimtext bei drücken des Tausch-Buttons
    * @param {event} evt Click-Event auf dem Tauschen-Button
    */
    tauschBtn.switchContainer.on("click", function(evt){
        clickCounter++;
        
        var klarText = document.getElementById("inputField").value;
        var geheimText = document.getElementById("output").value;
        document.getElementById("inputField").value = geheimText;
        document.getElementById("output").value = klarText;
        
        if(clickCounter % 2 === 1){
            klartxt.text = "Geheimtext:";
            klartxt.color = red;
            klartxt.x = verschluesselt.x;
        
            verschluesselt.text = "Klartext:";
            verschluesselt.color = tuerkis;
            verschluesselt.x = klartxt.x;
        } else {
            klartxt.text = "Klartext:";
            klartxt.color = tuerkis;
            klartxt.x = klartxt.x;
        
            verschluesselt.text = "Geheimtext:";
            verschluesselt.color = red;
            verschluesselt.x = verschluesselt.x;
        }
    });
    
    /**Prueft ob Weiter-Button noch vorhanden ist oder noch eine Animation läuft,
    wenn Verschlüsseln-Button erneut gedrückt wird. Falls ja wird der Weiter-Button
    entfernt und die Animation beendet
    * @param {event} evt Click-Event auf den Verschlüsseln-Button
    */
    
    buttonEncryp.encrypBtnContainer.on("click", function(evt){
        if(animateCaesar){
        animateCaesar.animationContainer.removeAllChildren();
        stage.removeChild(animateCaesar.animationContainer);
      }
        if(weiterButton){
            stage.removeChild(weiterButton.weiterContainer);
        }
        stage.update();
    });
    
    /** Event-Handler für clicken des Verschlüsseln-Buttons. 
        Ruft die Funktion für die Verschlüsselung des eingegebenen Textes auf.
    */
    buttonEncryp.encrypBtnContainer.addEventListener("click", caesarVerschluesselung);
    
     /**Prüft ob Weiter-Button noch vorhanden ist oder noch eine Animation läuft,
    wenn Entschlüsseln-Button erneut gedrückt wird. Falls ja wird der Weiter-Button
    entfernt und die Animation beendet
    * @param {event} evt Click-Event auf den Entschlüsseln-Button
    */
    buttonDecryp.decrypBtnContainer.on("click", function(evt){
        if(animateCaesar){
        animateCaesar.animationContainer.removeAllChildren();
        stage.removeChild(animateCaesar.animationContainer);
      }
        if(weiterButton){
            stage.removeChild(weiterButton.weiterContainer);
        }
        stage.update();
    });
    
    /** Event-Handler für clicken des Entschlüsseln-Buttons.
        Ruft die Funktion für die Entschlüsselung des eingebenen Textes auf.
    */
    buttonDecryp.decrypBtnContainer.addEventListener("click", caeasarEntschluesselung);
    
    stage.update();
 }
    /** Gibt die Startfunktion beim Caesar-Modul zurück
    * @returns {function} start Funktion zum starten des Caesar-Moduls
    */
    return {
        start:start
    };
})();

/** Erzeugt die Grafik für die Caesar-Scheibe
* @class
*/
function CaesarShape(){
    var thisArc;
    var angle = 0; angle2 = 0;
    
    this.container = new createjs.Container();
    this.outerTextContainer = new createjs.Container(); 
    
    this.circle = new createjs.Shape();
    this.circle.graphics.beginStroke(black).beginFill(tuerkis).drawCircle(0, 0, largeRadius);
    this.outerTextContainer.addChild(this.circle);
    
    var spacing = 360 / labelCaesar.length;
    
    this.outerTextContainer.x = (canvas.width/2) / scale;
    this.outerTextContainer.y = (canvas.height/2 - largeRadius/2 + offset/2) / scale;
    
    this.arcShape = new createjs.Shape(); 
    
    /** Abgrenzung der Buchstaben voneinander
    * {@link http://jsfiddle.net/lannymcnie/nJKHk/1/} */
    while(angle <= 360){
        thisArc = spacing;
        
        var startAngle = angle * Math.PI/180;
        var endAngle = Math.min(360, angle+thisArc)*Math.PI/180; 
        
        this.arcShape.graphics.s(black);
        this.arcShape.graphics.f(tuerkis);
        this.arcShape.graphics.moveTo(0,0)
        this.arcShape.graphics.arc(0,0,largeRadius, startAngle, endAngle);
        
        angle += thisArc;
        
        this.outerTextContainer.addChild(this.arcShape);
    }
    /**Text auf äusseren Ring setzen */
    for(var index = 0; index < labelCaesar.length; index++){
        this.outerRingText = new createjs.Text();
        this.outerRingText.text = labelCaesar[index];
        this.outerRingText.font = smallFont;
        if(labelCaesar[index] === "A"){
            this.outerRingText.font = mediumFont;
        }
        this.outerRingText.textAlign = "center";
        this.outerRingText.textBaseline = "middle";
        this.outerRingText.regY = Math.ceil(135 * scale) ;
        this.outerRingText.rotaion = (index - 1) * spacing;
        this.outerRingText.alpha = 0;
        
        createjs.Tween.get(this.outerRingText).to({rotation: index * spacing, alpha: 1}, 0, createjs.Ease.cubicOut);
        this.outerTextContainer.addChild(this.outerRingText);
    }
    this.innerTextContainer = new createjs.Container();
    this.innerTextContainer.name = "verContainer";
    
    this.outerRing = new createjs.Shape();
    this.outerRing.graphics.beginStroke(black).beginFill(red).drawCircle(0, 0, mediumRadius);
    this.innerTextContainer.addChild(this.outerRing);
    
    this.arcShape2 = new createjs.Shape();
    
/** Abgrenzug der Buchstaben voneinander */  
    while(angle2 <= 360){
        thisArc = spacing;
        
        var startAngle = angle2 * Math.PI/180;
        var endAngle = Math.min(360, angle2 + thisArc)*Math.PI/180; 
        
        this.arcShape2.graphics.s(black);
        this.arcShape2.graphics.f(transparentRed);
        this.arcShape2.graphics.moveTo(0,0)
        this.arcShape2.graphics.arc(0, 0, mediumRadius, startAngle, endAngle);
        
        angle2 += thisArc;
        
        this.innerTextContainer.addChild(this.arcShape2);
    }

    /**Text auf inneren Ring setzen */
    for(var index = 0; index < labelCaesar.length; index++){
        this.innerRingText = new createjs.Text();
        this.innerRingText.text = labelCaesar[index];
        this.innerRingText.font = smallFont;
        if(labelCaesar[index] === "A"){
            this.innerRingText.font = mediumFont;
        }
        this.innerRingText.textAlign = "center";
        this.innerRingText.textBaseline = "middle";
        this.innerRingText.regY = Math.ceil(105 * scale);
        this.innerRingText.rotaion = (index - 1) * spacing;
        this.innerRingText.alpha = 0;
        
    createjs.Tween.get(this.innerRingText).to({rotation: index * spacing, alpha: 1}, 0, createjs.Ease.cubicOut);
    /**Rotationspunkte speichern */
    if(initialRotation.length < 26){
        initialRotation.push(this.innerRingText.rotaion + spacing);
    }
    this.innerTextContainer.addChild(this.innerRingText);
    }

    this.innerTextContainer.x = (canvas.width/2) / scale;
    this.innerTextContainer.y = (canvas.height/2 - largeRadius/2 + offset/2) / scale;
    this.innerTextContainer.cursor = "pointer";

    
    this.animationContainer = new createjs.Container();
    this.animationContainer.x = this.innerTextContainer.x;
    this.animationContainer.y = this.innerTextContainer.y;
    this.animationContainer.setBounds(mediumRadius, mediumRadius);
    
    this.innerRing = new createjs.Shape();
    this.innerRing.graphics.beginStroke(black).beginFill(darkgrey).drawCircle(0, 0, smallRadius);
    this.innerRing.x = (canvas.width/2) / scale;
    this.innerRing.y = (canvas.height/2 - largeRadius/2 + offset/2) / scale;
    this.container.addChild(this.innerRing);
    
    this.whiteSpace = new createjs.Shape();
    this.whiteSpace.graphics.beginFill(grey).drawCircle(0, 0, tinyRadius);
    this.whiteSpace.x = (canvas.width/2) / scale;
    this.whiteSpace.y = (canvas.height/2 - largeRadius/2 + offset/2) / scale;
    this.container.addChild(this.whiteSpace);
    
    stage.update();
}

