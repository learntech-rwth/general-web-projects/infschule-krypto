/** http://stackoverflow.com/questions/9614109/how-to-calculate-an-angle-from-points */
/** Hilfsfunktion um bei Drehung des Containers die Ausrichtung des Fingers zu haben */

/** 
* Hilfsfunktion für die Drehung des Conainers, hilft die Ausrichtung zum Mittelpunkt zu haben
* @global
* @function rotationAngle
* @param {number} cx X-Koordinate des aktuellen Ziels
* @param {number} cy Y-Koordinate des aktuellen Ziels
* @param {number} ex Position auf de X-Achse im Verhältnis zur Stage
* @param {number} ey Position auf de Y-Achse im Verhältnis zur Stage
* @returns {number} theta Winkel zwischen aktueller Koordinate des Ziels und normalisierter Position im Verhältnis zur Stage
*/
function rotationAngle(cx, cy, ex, ey) {
  var theta = angle(cx, cy, ex, ey); // range (-180, 180]
  if (theta < 0){
	  theta = 360 + theta -90;
  }else{
	  theta-= 90;
  }	
  return theta;
}

/** 
* Hilfsfunktion für Funktion rotationAngle
* @global
* @funtion angle
* @param {number} cx X-Koordinate des aktuellen Ziels
* @param {number} cy Y-Koordinate des aktuellen Ziels
* @param {number} ex Position auf de X-Achse im Verhältnis zur Stage
* @param {number} ey Position auf de Y-Achse im Verhältnis zur Stage
* @returns {number} theta Winkel in Grad zwischen aktueller Koordinate des Ziels und normalisierter Position im Verhältnis zur Stage
*/
function angle(cx, cy, ex, ey) {
  var dy = ey - cy;
  var dx = ex - cx;
  var theta = Math.atan2(dy, dx); // range (-PI, PI]
  theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
  return theta;
}

