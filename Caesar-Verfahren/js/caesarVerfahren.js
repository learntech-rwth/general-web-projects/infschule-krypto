/**
* Verschluesselt den Klartext und ruft die Funktion fuer das Abspielen der Animation und die Ausgabe der Lösung auf.
* @global
* @function caesarVerschluesselung
* @param {event} evt Click-Event auf den Verschlüsseln-Button
*/

function caesarVerschluesselung(evt){
    var input = [];
    var index = 0;
    var result = [];
    var verschiebung = 0;
    var plainText = document.getElementById("inputField").value;
    
    clearTimeout(timer);
    verschiebung = caesarVerschiebung % 26;
    
    if(verschiebung === 0){
        alert("Drehe den inneren Ring der Caesar-Scheibe um die Verschiebung des Alphabets einzustellen.");
        document.getElementById("verschiebung").value = caesarVerschiebung;
        return false;
    } else {
        document.getElementById("verschiebung").value = caesarVerschiebung;
    }
    
    if(plainText == "" || plainText == " "){
        alert("Bitte Text eingeben.");
        return false;
    }
    
    var output = "";
    /** Hilfsvariablen */
    var res, resf;
   
/** Übersetzen in Großbuchstaben wegen Caesar-Scheibe */   
    plainText = plainText.toUpperCase();
    document.getElementById("inputField").value = plainText;
    for(var i = 0; i < plainText.length; i++){
        input.push(plainText[i]);
        res = plainText.charCodeAt(i);
        if(checkChar(res) === false){
            alert("Bitte nur Buchstaben von A - Z eingeben!")
            return false;
        }
        if(64 < res && res < 91){
            resf = res + verschiebung;
            if(resf > 90)
                resf = resf - 90 + 64;
               output = String.fromCharCode(resf);
               result.push(output);
            } else {
                output = plainText[i];
                result.push(output);
            }
        }
   
    crypAnimation(index, input, result);
}

/** 
* Spielt die Animation ab und gibt das Erbebnis der Verschlüsselung aus
* @global
* @function crypAnimation
* @param {number} index Aktueller Index im Array für die Eingabe und für das Ergebnis
* @param {string[]} input Enthält Buchstaben der Eingabe
* @param {string[]} result Enthält Buchstaben der Lösung
*/
function crypAnimation(index, input, result){ 
    
      animateCaesar = new CaesarShape();
    
      if(result.length > 1){
        weiterButton = new WeiterBtn();
        weiterButton.weiterContainer.x = (canvas.width/2 + crypBtnWidth/2 + offset) / scale;
        weiterButton.weiterContainer.y = (canvas.height/(1.1)) / scale;
        stage.addChild(weiterButton.weiterContainer);
    }
    
    document.getElementById("output").value = result[index];
    /** Animation für die Verschlüsselung */
        for(j = 0; j < ringChars.length; j++){
            if(input[index] == ringChars[j]){
                
                var arcShape = new createjs.Shape();
                arcShape.graphics.beginStroke(black).beginFill(red).setStrokeStyle(2).arc(0, 0, arcRadius, 70 * (Math.PI/180), 110 * (Math.PI/180)).lineTo(0,0).closePath();
                arcShape.alpha = 0;
                arcShape.rotaion = labelRotation[j];
                arcShape.regY = Math.ceil(150 * scale);
                createjs.Tween.get(arcShape, {overwrite: true}).to({rotation: arcShape.rotaion, alpha:0}, 250, createjs.Ease.cubicOut).call(handleComplete);
                    function handleComplete(){
                        createjs.Tween.get(arcShape, {overwrite: true}).to({alpha:0.4}, 2500, createjs.Ease.cubicOut);
                     }
                animateCaesar.animationContainer.addChild(arcShape);
                stage.addChild(animateCaesar.animationContainer);
                stage.update();
                    if(result.length === 1){
                        removeShape(animateCaesar.animationContainer);
                    }
                
                break;
            }
        }
                  /** 
                  * Bei drücken des Weiter-Buttons wird nächste Animation abgespielt und nächster Buchstabe der Lösung ausgegeben.
                  * @param {event} evt Click-Event auf den Weiter-Button
                  */
                   weiterButton.weiterContainer.on("click", function(evt){
                       var wb = evt.currentTarget;
                      
                       clearTimeout(timer);
                       /** Container leeren */
                       animateCaesar.animationContainer.removeAllChildren();
                       ++index;
                       if(input[index] === " "){
                           document.getElementById("output").value += " ";
                       }
                       for(j = 0; j < ringChars.length; j++){
                           if(input[index] == ringChars[j]){
                               var arcShape = new createjs.Shape();
                               arcShape.graphics.beginStroke(black).beginFill(red).setStrokeStyle(2).arc(0, 0, arcRadius, 70 * (Math.PI/180), 110 * (Math.PI/180)).lineTo(0,0).closePath();
                                arcShape.alpha = 0;
                                arcShape.rotaion = labelRotation[j];
                                arcShape.regY = Math.ceil(150 * scale);
                                createjs.Tween.get(arcShape, {overwrite: true}).to({rotation: arcShape.rotaion, alpha: 0}, 250, createjs.Ease.cubicOut).call(handleComplete);
                                    function handleComplete(){
                                        createjs.Tween.get(arcShape, {overwrite: true}).to({alpha:0.4}, 2500, createjs.Ease.cubicOut);
                                     }
        
                                animateCaesar.animationContainer.addChild(arcShape);
                                stage.addChild(animateCaesar.animationContainer);
                                stage.update();
                                
                                document.getElementById("output").value += result[index];
                               
                               if((index + 1) == result.length){
                                   removeBtn(wb);
                                   removeShape(animateCaesar.animationContainer, arcShape);
                               }
                               
                               break;
                           }
                       }
                   });   
   
}

/**
* Entschlüsselt den Geheimtext und ruft die Funktion für das Abspielen der Animation und die Ausgabe der Lösung auf.
* @global
* @function caesarEntschluesselung
* @param {event} evt Click-Event auf den Entschlüsseln-Button
*/
function caeasarEntschluesselung(evt){
    var input = [];
    var index = 0;
    var result = [];
    var verschiebung = 0;
    var cipherText = document.getElementById("inputField").value;
    document.getElementById("output").value = "";
    

    clearTimeout(timer);
   
    verschiebung = (caesarVerschiebung * (-1)) + 26;
    
    document.getElementById("verschiebung").value = caesarVerschiebung;
    
    var output = "";
    /** Hilfsvariablen */
    var res, resf;
   
/** Übersetzen in Großbuchstaben wegen Caesar-Scheibe */   
    cipherText = cipherText.toUpperCase();
    document.getElementById("inputField").value = cipherText;
    for(var i = 0; i < cipherText.length; i++){
        input.push(cipherText[i]);
        res = cipherText.charCodeAt(i);
        if(checkChar(res) === false){
            alert("Bitte nur Buchstaben eingeben!")
            return false;
        }
        if(64 < res && res < 91){
            resf = res + verschiebung;
            if(resf > 90)
                resf = resf - 90 + 64;
               output = String.fromCharCode(resf);
               result.push(output);
            } else {
                output = cipherText[i];
                result.push(output);
            }
        }
    
    encrypAnimation(index, input, result);
}

/** 
* Spielt die Animation ab und gibt das Erbebnis der Entschlüsselung aus
* @global
* @function encrypAnimation
* @param {number} index Aktueller Index im Array für die Eingabe und fuer das Ergebnis
* @param {string[]} input Enthält Buchstaben der Eingabe
* @param {string[]} result Enthält Buchstaben der Lösung
*/
function encrypAnimation(index, input, result){
    animateCaesar = new CaesarShape();
    
    if(result.length > 1){
        weiterButton = new WeiterBtn();
        weiterButton.weiterContainer.x = (canvas.width/2 + crypBtnWidth/2 + offset) / scale;
        weiterButton.weiterContainer.y = (canvas.height/(1.1)) / scale;
        stage.addChild(weiterButton.weiterContainer);
    }
    
     document.getElementById("output").value = result[index];
    
     /** Animation fuer die Entschlüsselung */
        for(j = 0; j < ringChars.length; j++){
            if(input[index] == ringChars[j]){
                
                var arcShape = new createjs.Shape();
                arcShape.graphics.beginStroke(black).beginFill(blue).setStrokeStyle(2).arc(0, 0, arcRadius, 250 * (Math.PI/180), 290 * (Math.PI/180)).lineTo(0,0).closePath();
                //arcShape.graphics.lt(0,0);
                arcShape.alpha = 0;
                arcShape.rotaion = initialRotation[j];
                arcShape.regY = Math.ceil(90 * scale);
                createjs.Tween.get(arcShape, {overwrite: true}).to({rotation: arcShape.rotaion, alpha: 0}, 250, createjs.Ease.cubicOut).call(handleComplete);
                    function handleComplete(){
                         createjs.Tween.get(arcShape, {overwrite: true}).to({alpha:0.4}, 2500, createjs.Ease.cubicOut); 
                     }
        
                animateCaesar.animationContainer.addChild(arcShape);
                stage.addChild(animateCaesar.animationContainer);
                stage.update();
             
                     if(result.length === 1){
                        removeShape(animateCaesar.animationContainer);
                    }
                
                break;
            }
        }
                  /** 
                  * Bei drücken des Weiter-Buttons wird nächste Animation abgespielt und nächster Buchstabe der Lösung ausgegeben.
                  * @param {event} evt Click-Event auf den Weiter-Button
                  */
                   weiterButton.weiterContainer.on("click", function(evt){
                       var wb = evt.currentTarget;
                       
                       
                       clearTimeout(timer);
                       /** Container leeren */
                       animateCaesar.animationContainer.removeAllChildren();
                       ++index;
                       if(input[index] === " "){
                           document.getElementById("output").value += " ";
                       }
                       for(j = 0; j < ringChars.length; j++){
                           if(input[index] == ringChars[j]){
                               var arcShape = new createjs.Shape();
                               arcShape.graphics.beginStroke(black).setStrokeStyle(2).beginFill(blue).arc(0, 0, arcRadius, 250 * (Math.PI/180), 290 * (Math.PI/180)).lineTo(0,0).closePath();
                                //arcShape.graphics.lt(0,0);
                                arcShape.alpha = 0;
                                arcShape.rotaion = initialRotation[j];
                                arcShape.regY = Math.ceil(90 * scale);
                                createjs.Tween.get(arcShape, {overwrite: true}).to({rotation: arcShape.rotaion, alpha: 0}, 250, createjs.Ease.cubicOut).call(handleComplete);
                                    function handleComplete(){
                                        createjs.Tween.get(arcShape, {overwrite: true}).to({alpha:0.4}, 2500, createjs.Ease.cubicOut); 
                                     }
        
                                animateCaesar.animationContainer.addChild(arcShape);
                                stage.addChild(animateCaesar.animationContainer);
                                stage.update();
                               
                                document.getElementById("output").value += result[index];
                               
                               if((index + 1) == result.length){
                                   removeBtn(wb);
                                   removeShape(animateCaesar.animationContainer, arcShape);
                               }
                               
                               break;
                           }
                       }
                   });   
   
}

/** Hilfsfunktionen */

/**
* Testet ob die Eingabe aus Buchstaben von A - z oder aus Leerzeichen besteht
* @global
* @function checkChar
* @param {number} text Char Code des aktuellen Buchstaben des Eingabetextes
* @returns {boolean} Gibt true zurück falls Char Code im Bereich von 65 - 90 oder 32, sonst false 
*/
function checkChar(text){
    return (text === 32) || (65 <= text && text <= 90);
}

 /** 
 * Entfernt den Pfeil auf der Caesar-Scheibe nach 6 Sekunden
 * @global
 * @function removeShape
 * @param {Object} container Container-Object das zur Caesar-Scheibe hinzugefügt wurde
 * @param {Object} shape Shape-Object das für die Animation erzeugt wurde
 */
function removeShape(container, shape){
    timer = setTimeout(function(){
        createjs.Tween.removeAllTweens();                   
        stage.removeChild(container, shape);
    }, 6000);  
}

/** 
* Entfernt einen Button
* @global
* @function removeBtn
* @param {Object} btn Button-Object das übergeben wurde
*/
function removeBtn(btn){
        stage.removeChild(btn);
}
