/** Globale Variablen */

var canvas;
var stage;

/** Wird für Timeout-Function benötigt */
var timer;
/** Wird für die Skallierung benötigt */
var scale = 0; 

var menueBtnWidth; 
var menueBtnHeight;  

var zurueckBtnWidth; 
var zurueckBtnHeight;

var crypBtnWidth; 
var crypBtnHeight; 

var switchBtnWidth;
var switchBtnHeight;

var offset; 

/** Wird für Tausch-Button benötigt */
var clickCounter = 0;

var tinyFont;
var smallFont;
var mediumFont;
var largeFont;
var hugeFont;


var white = "#fff"; 
var black = "#000";
var red = "#b82929";
var transparentRed = "rgba(184, 41, 41, 0.1)";
var blue = "#0077b6";
var tuerkis = "#66b8a0";
var yellow = "#fccb00";
var grey = "#f5f5f5";
var darkgrey = "#c0c0c0"


var largeRadius;
var mediumRadius;
var smallRadius;
var tinyRadius;
var arcRadius;

/** Container für Animationen, bei Caesar-Verfahren */
var animateCaesar; 
/** Container für Weiter-Button */
var weiterBtn; 
var weiterButton;

var labelCaesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var labelArray = [];
var chars = [];
/** Hintergrund */
var bg; 
/** Verschiebung des Alphabet bei Caesar-Verfahren */
var caesarVerschiebung = 0; 
/** Rotaionspunkte der Buchstaben des äußeren Rings */
var initialRotation = [];
/** Rotaionspunkte der Buchstaben des inneren Rings */
var labelRotation = [];
/** Enthaelt Buchstaben von A - Z */
var ringChars = [];

/**
* Laedt Bilder und startet das Hauptmenü wenn der Ladevorgang komplett ist
* @function setup
*/
function setup(){
        var preload = new createjs.LoadQueue();
        preload.on("complete", startApp);
        preload.loadFile("images/arrow-down.png");
        preload.loadFile("images/arrow-up.png");
}

/**
* Startet die Webanwendung
* @global
* @function startApp
*/
function startApp(){
   var app = new App(); 
   
}
/**
* Legt wichtige Werte wie die Canvas-Größe Fest und startet das jeweilige Modul für die Chiffrierverfahren
* @class
* @namespace App
*/
function App(){
    canvas = document.getElementById("cipherCanvas");
   
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    
    stage = new createjs.Stage("cipherCanvas");
    
/**
* Behandelt das Ändern der Fenstergröße im Browser
* @function onResize
*/
   window.onResize = function()
   {
    canvas.width = window.innerWidth;
    canvas.style.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.height = window.innerHeight;
   }
    
    createjs.Touch.enable(stage);
    stage.enableMouseOver();
    
    createjs.Ticker.setFPS(30);       
    createjs.Ticker.addEventListener("tick", tick);
    
    //window.addEventListener("resize", handleResize);
    //window.addEventListener('orientationchange', handleResize);
     
    for(i = 0; i < labelCaesar.length; i++){
        labelArray.push(labelCaesar[i]);
    }
    
    scale = 1; /** default */
   //scale = Math.min(window.innerWidth/window.innerHeight, window.innerHeight/window.innerWidth);
    if(window.devicePixelRatio >= 2 && window.devicePixelRatio <= 3){
        scale = 0.8;
    }
    if(window.devicePixelRatio === 1.25 || screen.width < 400){
        scale = 0.8;
    }
    if(window.devicePixelRatio > 3){
        scale = 0.8;
    }
    if(window.devicePixelRatio == 2 && screen.width > 1000){
        scale = 1;
    }
    /** Kleines Fenster oder Bildschirm */
    if(screen.width <= 300 || window.innerWidth <= 300){
        scale = 0.65;
    }
    canvas.style.width = canvas.width * scale;
    canvas.style.height = canvas.height * scale;
    
    stage.canvas.width = canvas.width;
    stage.canvas.height = canvas.height;
    
    stage.scaleX = scale;
    stage.scaleY = scale;
     
    zurueckBtnWidth = Math.ceil(75 * scale);
    zurueckBtnHeight = Math.ceil(30 * scale);
    
    menueBtnWidth = Math.ceil(scale * 400);
    menueBtnHeight = Math.ceil(scale * 50);
    
    crypBtnWidth = Math.ceil(120 * scale);
    crypBtnHeight = Math.ceil(30 * scale);
    
    switchBtnWidth = Math.ceil(scale * 50);
    switchBtnHeight = Math.ceil(scale * 30);
    
    largeRadius = Math.ceil(scale * 150);
    mediumRadius = Math.ceil(scale * 120);
    smallRadius = Math.ceil(scale * 90);
    tinyRadius = Math.ceil(scale * 89);
    arcRadius = Math.ceil(scale * 60);
    
    offset = Math.ceil(scale * 20);
    
    tinyFont = "bold " + Math.ceil(12 * scale) + "px monospace";
    smallFont = "bold " + Math.ceil(14 * scale) + "px monospace";
    mediumFont = "bold " + Math.ceil(18 * scale) + "px monospace";
    largeFont = "bold " + Math.ceil(24 * scale) + "px  monospace";
    hugeFont = "bold " + Math.ceil(28 * scale) + "px monospace";
        
    /** Starte Modul für das Caesar-Verfahren */
    caesarModule.start();
          
    stage.update();
}


/**
* Updated Stage bei jedem Tick des Timers
* @global
* @function tick
* @param {event} event Tick-Event des Timers
*/
function tick(event) {
    stage.update(event); 
}
