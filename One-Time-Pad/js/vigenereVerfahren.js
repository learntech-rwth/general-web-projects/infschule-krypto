/** Bereitet die Eingabe vor. Falls der Text bereits verschlüsselt ist
wird der Schlüssel für die Entschlüsselung entsprechend angepasst.
* Ruft außerdem die Funktion für die Ver- oder Entschlüsselung des Texts auf.
* @memberof vigenereModule
* @function checkVerschluesseln
* @param {boolean} decrypted Falls der Text bereits verschlüsselt ist true, sonst false
*/
function checkVerschluesseln(decrypted){
    clearTimeout(timer);
    var input = [];
    var keyChars = [];
    var result = [];
    var key = document.getElementById("key").value;
    var eingabe = document.getElementById("inputField").value;
    
     if(oneTimePad){
        if(key.length !== eingabe.length){
            alert("Der Schlüssel muss so lang wie die Eingabe sein!");
            return false;
        }
    }
    
    if(key === ""){
        alert("Der Schlüssel darf nicht leer sein!");
        return false;
    }
    if(eingabe === "" || eingabe === " "){
        alert("Bitte Text eingeben!");
        return false;
    }
    
     for(i = 0; i < eingabe.length; i++){
           for(j = 0; j < key.length; j++){
               if(i == j){
                   if(eingabe[i] == " " && key[j] != " " || eingabe[i] != " " && key[j] == " "){
                       alert("Es dürfen nur Buchstaben von A - z eingeben werden, keine Leerzeichen.")
                       return false;
                   }
               }
           }
       }
    
    key = key.toUpperCase();
    document.getElementById("key").value = key;
     
    for(i = 0; i < key.length; i++){
        keyChars.push(key[i]);
    }
    
    /** Schluessel auf Laenge der Eingabe abbilden */
    if(!oneTimePad){
        var index = 0;
        document.getElementById("key").value = "";
        for(i = 0; i < eingabe.length; i++){
              if(index == keyChars.length){
                  index = 0;
              }
              if(eingabe[i] == " "){
                  document.getElementById("key").value += " ";
                  index--;
              } else {
                  document.getElementById("key").value += keyChars[index]; 
              }
             index++;
        }
    }
        
    
    /** Pruefe fuer One-Time-Pad ob Schluessel schon verwendet wurde */
    if(oneTimePad && !decrypted){
        for(a = 0; a < oneTimeKeys.length; a++){
                if(oneTimeKeys[a] === key){
                    alert("Aus Sicherheitsgründen darf nicht mehrmals der selbe Schlüssel verwendet werden.");
                    return false;
                }
        }
        
        oneTimeKeys.push(key);
    }
    var keyLetters = checkKey(key);
    
    /** Eingabe wegen Vigenére-Quadrat zu Großbuchstaben konvertieren */
    eingabe = eingabe.toUpperCase();
    document.getElementById("inputField").value = eingabe;
    for(i = 0; i < eingabe.length; i++){
        input.push(eingabe[i]);
    }
    
    if (decrypted) {
		for (var i = 0; i < keyLetters.length; i++)
			keyLetters[i] = (26 - keyLetters[i]) % 26;
	}
    verschluesseln(eingabe, keyLetters, keyChars, input, result, decrypted);
}

/**
* Verschlüsselt den Text falls die Variable decrypted den Wert true hat, sonst wird entschlüsselt.
* Ruft außerdem die jeweilige Funktion für das Abspielen der Animation und die Ausgabe der Lösung auf. 
* @memberof vigenereModule
* @function verschluesseln
* @param {string} text Klar- oder Geheimtext der eingegeben wurde
* @param {string[]} keyLetters Enthält Buchstaben des Schlüssels, wird für Berechnung des Ergebnis benötigt
* @param {string[]} keyChars Enthält Buchstaben des Schlüssels, wird für Animation benötigt
* @param {string[]} input Enthält Buchstaben der Eingabe
* @param {string[]} result Enthält Buchstaben der Lösung
* @pram {boolean} decrypted Wahr wenn Text bereits verschlüsselt, sonst falsch
*/
function verschluesseln(text, key, keyChars, input, result, decrypted){
    var index = 0;
    var output = "";
   
    for(var i = 0, j = 0; i < text.length; i++) {
		var char = text.charCodeAt(i);
        if(checkLetters(char) == true) {
            // Leerzeichen im Schluessel bei One-Time-Pad-Verfahren behandeln
            if(oneTimePad && char == 32){
                //output = text.charAt(i); 
                //result.push(output);
                //j++;
                alert("Der Text darf keine Leerzeichen enthalten");
                return false;
            } 
            if(char !== 32){
            output = String.fromCharCode((char - 65 + key[j % key.length]) % 26 + 65);
            result.push(output);
			j++;
            }
        } else {
            //output = text.charAt(i);
            //result.push(output);
            alert("Bitte nur Buchstaben von A - Z eingeben!");
            return false;
		}
    }
    document.getElementById("output").value = result[index]; 
    
    if(!decrypted){
       animateEncryp(index, keyChars, input, result);
    } else {
       animateDecryp(index, keyChars, input, result);
    }
}

/**
* Spielt die Animation ab und gibt das Ergebnis der Verschlüsselung aus.
* @memberof vigenereModule
* @function animateEncryp
* @param {number} index Aktueller Index im Array für die Eingabe und für das Ergebnis
* @param {string[]} keyChars Enthält Buchstaben des Schlüssels
* @param {string[]} input Enthält Buchstaben der Eingabe
* @param {string[]} result Enthält Buchstaben der Lösung
*/
function animateEncryp(index, keyChars, input, result){
    if(!animateContainer){
    animateContainer = new VigenereShape();
    }
    keyIndex = 0;
        
    for(i = 0; i < labelArray.length; i++){
        
            if(keyChars[keyIndex] == labelArray[i]){
                var rectangle = new createjs.Shape();
                rectangle.graphics.beginStroke(black).beginFill(red).drawRect(0, 0, textWidth + textHeight, textHeight*(0.8));
                rectangle.alpha = 0;
                rectangle.x = keyX[i] - offset/2;
                rectangle.y = keyY[i] - offset/2;
                createjs.Tween.get(rectangle).to({alpha: 0.4}, 5000, createjs.Ease.cubicOut);
                animateContainer.animationContainer.addChild(rectangle);
                stage.addChild(animateContainer.animationContainer);
                stage.update();
           
            break;
            }

        }
    
    for(i = 0; i < labelArray.length; i++){
       
            if(input[index] == labelArray[i]){
                var rectangle = new createjs.Shape();
                rectangle.graphics.beginStroke(black).beginFill(red).drawRect(0, 0, textHeight * (0.8), textWidth + textHeight);
                rectangle.alpha = 0;
                rectangle.x = textX[i] - offset/2;
                rectangle.y = textY[i] - offset/2;
                createjs.Tween.get(rectangle).to({alpha: 0.4}, 3000, createjs.Ease.cubicOut);
                animateContainer.animationContainer.addChild(rectangle);
                stage.addChild(animateContainer.animationContainer);
                stage.update();
           
            break;
            }

        }
    
    if(result.length === 1){
        removeContainer(animateContainer.animationContainer);
    }
    
    if(result.length > 1){
        weiterBtn = new WeiterBtn();
        weiterBtn.weiterContainer.x = (canvas.width/2 + crypBtnWidth/2 + offset) / scale;
        weiterBtn.weiterContainer.y = (canvas.height/(1.1)) / scale;
        stage.addChild(weiterBtn.weiterContainer);
    }
    
    /** 
     * Bei drücken des Weiter-Buttons wird nächste Animation abgespielt und nächster Buchstabe der Lösung ausgegeben.
     * @param {event} evt Click-Event auf den Weiter-Button
     */
    weiterBtn.weiterContainer.on("click", function(evt){
        var wb = evt.currentTarget;
        clearTimeout(timer);
        animateContainer.animationContainer.removeAllChildren();
        index++;
        keyIndex++;
        if(keyIndex === keyChars.length){
            keyIndex = 0;
        }
        /** Falls Leerzeichen vorhanden */
        for(i = 0; i < labelArray.length; i++){
           if(input[index] ==" "){
               if(!oneTimePad){
               keyIndex--;
               break;
               }
               if(oneTimePad){
                   break;
               }
           }
            if(keyChars[keyIndex] == labelArray[i]){
                var rectangle = new createjs.Shape();
                rectangle.graphics.beginStroke(black).beginFill(red).drawRect(0, 0, textWidth + textHeight, textHeight*(0.8));
                rectangle.alpha = 0;
                rectangle.x = keyX[i] - textHeight/2;
                rectangle.y = keyY[i] - textHeight/2;
                createjs.Tween.get(rectangle).to({alpha: 0.4}, 5000, createjs.Ease.cubicOut);
                animateContainer.animationContainer.addChild(rectangle);
                stage.addChild(animateContainer.animationContainer);
                stage.update();
          
            break;
            }

        }
        
        for(i = 0; i < labelArray.length; i++){
           
            if(input[index] == labelArray[i]){
                var rectangle = new createjs.Shape();
                rectangle.graphics.beginStroke(black).beginFill(red).drawRect(0, 0, textHeight * (0.8), textWidth + textHeight);
                rectangle.alpha = 0;
                rectangle.x = textX[i] - textHeight/2;
                rectangle.y = textY[i] - textHeight/2;
                createjs.Tween.get(rectangle).to({alpha: 0.4}, 3000, createjs.Ease.cubicOut);
                animateContainer.animationContainer.addChild(rectangle);
                stage.addChild(animateContainer.animationContainer);
                stage.update();
            
            break;
            }

        }
        document.getElementById("output").value += result[index];
        
         if((index + 1) === result.length){
            removeBtn(wb);
            document.getElementById("key").value = "";
            for(i = 0; i < keyChars.length; i++){
             document.getElementById("key").value += keyChars[i];
             }
            keyChars = [];
            removeContainer(animateContainer.animationContainer);
          }
        });
}
/**
* Spielt die Animation ab und gibt das Ergebnis der Entschlüsselung aus.
* @memberof vigenereModule
* @function animateDecryp
* @param {number} index Aktueller Index im Array für die Eingabe und für das Ergebnis
* @param {string[]} keyChars Enthält Buchstaben des Schlüssels
* @param {string[]} input Enthält Buchstaben der Eingabe
* @param {string[]} result Enthält Buchstaben der Lösung
*/
function animateDecryp(index, keyChars, input, result){
    var reihe = 0;
    var spalte = 0;
     if(!animateContainer2){
    animateContainer2 = new VigenereShape();
    }
    keyIndex = 0;
    
    /** Reihe und Spalte berechnen */
    for(i = 0; i < labelArray.length; i++){
            if(keyChars[keyIndex] == labelArray[i]){
                reihe = i;
              break;
            }  
    }
    
    for(i = 0; i < labelArray.length; i++){
            if(result[index] == labelArray[i]){
                spalte = i;
              break;
            }  
    }
    
     for(i = 0; i < labelArray.length; i++){
            if(keyChars[keyIndex] == labelArray[i]){
                var rectangle = new createjs.Shape();
                rectangle.graphics.beginStroke(black).beginFill(tuerkis).drawRect(0, 0, squareChars[reihe][spalte] + offset/4, textHeight*(0.8));
                rectangle.alpha = 0;
                rectangle.x = keyX[i] - textHeight/2;
                rectangle.y = keyY[i] - textHeight/2;
                createjs.Tween.get(rectangle).to({alpha: 0.4}, 2000, createjs.Ease.cubicOut).call(handleComplete);
                animateContainer2.animationContainer.addChild(rectangle);
                stage.addChild(animateContainer2.animationContainer);
                stage.update();
            function handleComplete(){
                for(i = 0; i < labelArray.length; i++){
                    if(result[index] == labelArray[i]){
                        var rectangle = new createjs.Shape();
                        rectangle.graphics.beginStroke(black).beginFill(tuerkis).drawRect(0, 0, textHeight * (0.8), textWidth - (keyY[25] - keyY[reihe]));
                        rectangle.alpha = 0;
                        rectangle.x = textX[i] - textHeight/2;
                        rectangle.y = textY[i] - textHeight/2;
                        createjs.Tween.get(rectangle).to({alpha: 0.4}, 2000, createjs.Ease.cubicOut);
                        animateContainer2.animationContainer.addChild(rectangle);
                        stage.update();
                        
                        if(result.length === 1){
                            removeContainer(animateContainer2.animationContainer);
                        }
            }
            }
            }
            break;
            }
     }
    
    if(result.length > 1){
        weiterBtn = new WeiterBtn();
        weiterBtn.weiterContainer.x = (canvas.width/2 + crypBtnWidth/2 + offset) / scale;
        weiterBtn.weiterContainer.y = (canvas.height/(1.1)) / scale;
        stage.addChild(weiterBtn.weiterContainer);
    }
    
    /** 
    * Bei drücken des Weiter-Buttons wird nächste Animation abgespielt und nächster Buchstabe der Lösung ausgegeben.
    * @param {event} evt Click-Event auf den Weiter-Button
    */
     weiterBtn.weiterContainer.on("click", function(evt){
        var wb = evt.currentTarget;
        clearTimeout(timer);
        animateContainer2.animationContainer.removeAllChildren();
        index++;
        keyIndex++;
         
        if(keyIndex === keyChars.length){
            keyIndex = 0;
        }
            
         /** Reihe und Spalte berechnen */
    for(i = 0; i < labelArray.length; i++){
            if(keyChars[keyIndex] == labelArray[i]){
                reihe = i;
              break;
            }  
    }
    
    for(i = 0; i < labelArray.length; i++){
            if(result[index] == labelArray[i]){
                spalte = i;
              break;
            }  
    }
    for(i = 0; i < labelArray.length; i++){
           if(result[index] === " "){
               if(!oneTimePad){
                   keyIndex--;
                   break;
                 }
               if(oneTimePad)
                   break;
           }
        
            if(keyChars[keyIndex] == labelArray[i]){
                var rectangle = new createjs.Shape();
                rectangle.graphics.beginStroke(black).beginFill(tuerkis).drawRect(0, 0, squareChars[reihe][spalte] + offset/4, textHeight*(0.8));
                rectangle.alpha = 0;
                rectangle.x = keyX[i] - textHeight/2;
                rectangle.y = keyY[i] - textHeight/2;
                createjs.Tween.get(rectangle).to({alpha: 0.4}, 2000, createjs.Ease.cubicOut).call(handleComplete);
                animateContainer2.animationContainer.addChild(rectangle);
                stage.addChild(animateContainer2.animationContainer);
                stage.update();
            function handleComplete(){
                for(i = 0; i < labelArray.length; i++){
                    if(result[index] == labelArray[i]){
                        var rectangle = new createjs.Shape();
                        rectangle.graphics.beginStroke(black).beginFill(tuerkis).drawRect(0, 0, textHeight * (0.8), textWidth - (keyY[25] - keyY[reihe]));
                        rectangle.alpha = 0;
                        rectangle.x = textX[i] - textHeight/2;
                        rectangle.y = textY[i] - textHeight/2;
                        createjs.Tween.get(rectangle).to({alpha: 0.4}, 2000, createjs.Ease.cubicOut);
                        animateContainer2.animationContainer.addChild(rectangle);
                        stage.update();
                       
            }
            }
            }
            break;
            }
     }
            
             document.getElementById("output").value += result[index];
        
         if((index + 1) === result.length){
            removeBtn(wb);
            document.getElementById("key").value = "";
            for(i = 0; i < keyChars.length; i++){
                document.getElementById("key").value += keyChars[i];
              }
            keyChars = [];
            removeContainer(animateContainer2.animationContainer);
          }
  });
}
   
/** Hilfsfunktionen */
            
/**
* Prüft ob der Schlüssel aus den Buchstaben A - Z besteht. 
* Falls ja werden sie gespeichert, sonst wird eine Warnung ausgegeben.
* @global
* @function checkKey
* @param {string} key Der eingegebene Schlüssel
* @returns {string[]} letters Enthält die Buchstaben des Schlüssels
*/
function checkKey(key){
    var letters = [];
    for(var i = 0; i < key.length; i++) {
		var char = key.charCodeAt(i);
        if (checkLetters(char) == true){
			letters.push((char - 65) % 32);
        
}else {
    alert("Der Schlüssel muss aus Buchstaben (A - Z) bestehen!")
    return false;
}
    }
	return letters;
}

/**
* Prüft ob die Eingabe aus Buchstaben von A - z oder aus Leerzeichen enthält.
* @global
* @function checkLetters
* @param {number} char Char Code des aktuellen Buchstaben des Eingabetextes
* @return {boolean} Gibt true zurück falls Char Code im Bereich von 65 - 90 oder 32, sonst false 
*/
function checkLetters(char){
    if(oneTimePad){
        return (char == 32) || 65 <= char && char <= 90;
    }
    return 65 <= char && char <= 90;
}

/** 
 * Entfernt die Grafik auf dem Vigenére-Quadrat nach 6 Sekunden
 * @global
 * @function removeContainer
 * @param {Object} container Container-Object das zum Vigenére-Quadrat hinzugefügt wurde
 */
function removeContainer(container){
    timer = setTimeout(function(){
        createjs.Tween.removeAllTweens();                   
        stage.removeChild(container);
    }, 6000);
}

function removeBtn(btn){
        stage.removeChild(btn);
}

